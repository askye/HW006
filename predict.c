#include <stdio.h>
#include <stdlib.h>
#include <math.h>
char getAnswer();
int predict(int low, int high)
{
int low1=low; //saves original range
int high1=high; //saves original range
char str[120]; //for user input
fgets(str,120,stdin); //get user input
int numTries=(1+((log(1+high-low))/(log(2)))); //# of guesses algorithm
	printf("I will guess your number in %d tries\n",numTries); 
int guess=(low+high)/2; //guess algorithm
	printf("Guess 1: %d.\nIs this high(h), low(l) or correct(c)?",guess); //inital guess
for(int count=2;count<=numTries;count++){ //loop for remaining guesses
	char a=getAnswer(a); //a represents user's selection
//Cheat detection//
	if(low==high || guess>=high1 || guess<=low1 || count==numTries){
		printf("I think you're fibbing.\n"); //if user cheats
		exit(0);
	}
	else{	//Guess algorithm for correct responses//
		if(a=='h' || a=='l' || a=='c'){
			if(a=='h'){ //if high
				high=guess-1;
			}
			else if(a=='l'){ //if low
				low=guess+1;
			}
			else if(a=='c'){ //if correct
				printf("Thanks for playing!\n");
				exit(0);
			}
		guess=(low+high)/2;
		printf("Guess %d: %d.\nIs this high(h), low(l) or correct(c)?",count,guess);
		}
		else{
			printf("Please try again."); //error message
			count=count-1; //stays at current guess
		}	
	}
}	
}
