#include <stdio.h>
#include <math.h>
#include "main.h"

int main(int argc, char **argv)
{
int low=0;
int high=100;
char trash[120]; //string trash bin to parse real int
	if(argc==1){ //default range settings
		intro();
		numSelect(low,high);	
		predict(low,high);	
		getAnswer();
	}
	if(argc==2){ //user types high end of range into command line
		int ret=sscanf(argv[1],"%d""%s",&high,trash); //parse real int
		if(ret==1){
			intro();
			numSelect(low,high);
			predict(low,high);
			getAnswer();
		}
		else if(ret!=1){ //else, default range
			intro();
			numSelect(low,high);
			predict(low,high);
			getAnswer();
		}
	}
	else if(argc==3){ //user selects both low and high ends of range into  command line
		int ret=sscanf(argv[1],"%d""%s",&low,trash); //real int parsing
		int ret2=sscanf(argv[2],"%d""%s",&high,trash); //real int parsing
		if(ret==1 && ret2==1){
			if(low<high){ //range must follow this rule
				intro();
				numSelect(low,high);
				predict(low,high);
				getAnswer();
			}
		}
		else{
			intro();
			numSelect(low,high);
			predict(low,high);
			getAnswer();
		}
	}
return(0);
}
