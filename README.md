Name: Amethyst Skye
Class: CSE224-Programming Tools(Sec-A)
Date:10/29/2020
-------------------------------------------
Program Description: 
High/Low is a number guessing game in which the user thinks of a number between 0 and 100 (or any other range selected by the user). 
The CPU will then try to guess that number in a calculated amount of tries.
The CPU has the ability to detect any cheating ie. the user changing number in the middle of gameplay.
Once the CPU has guessed the correct number, or if the user has cheated, the game is over.
