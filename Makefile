main: main.o intro.o predict.o getAnswer.o numSelect.o
	gcc -o main main.o intro.o predict.o getAnswer.o numSelect.o -lm
main.o: main.c main.h
	gcc -c main.c -lm
intro.o: intro.c
	gcc -c intro.c
predict.o: predict.c
	gcc -c predict.c -lm
getAnswer.o: getAnswer.c
	gcc -c getAnswer.c
numSelect.o: numSelect.c
	gcc -c numSelect.c
clean:
	rm main main.o intro.o predict.o getAnswer.o numSelect.o
